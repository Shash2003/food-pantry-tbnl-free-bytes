package edu.trincoll.foodpantrytbnl;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DexMController {
    private final List<String> teamMembers =
            List.of("Shivanshu", "Armen", "Ryan", "Hung", "Chris", "Kamilla");

    @GetMapping("/dexm")
    public List<String> getTeamMembers() {
        return teamMembers;
    }
}
